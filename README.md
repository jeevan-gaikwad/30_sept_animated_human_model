# 30_Sept_animated_human_model

This repo hosts source code for our 30 Sept. 2018 demo.

Project is interactive human model(named Vertexa). She interacts with you in natural english language. She will respond you in natural language.
She has intellegence to answer your general informative questions. She also performs some actions based on your instructions.
The project content is set to our OpenGL RTR 2017 batch's demo presentation scheduled on 30-Sept-2018.

Build by: 
Team Vertex
(RTR 2017 Astromedicomp.org)

How to build:
Clone the VS 2015 project 'LoadingAndAnimation' and build it.