
#include <windows.h>
#include <iostream>
#include <mutex>
#include <dmo.h>
#include <Mmsystem.h>
#include <objbase.h>
#include <mediaobj.h>
#include <uuids.h>
#include <propidl.h>
#include <wmcodecdsp.h>

#include <atlbase.h>
#include <ATLComCli.h>
#include <audioclient.h>
#include <MMDeviceApi.h>
#include <AudioEngineEndPoint.h>
#include <DeviceTopology.h>
#include <propkey.h>
#include <strsafe.h>
#include <conio.h>
#include <stdlib.h>
#include "dmo_rec\mediabuf.h"
#include "dmo_rec\DMO_rec_binder.h"
#include "dmo_rec\DMO_rec.h"
#include <fstream>

/* Link required libraries. By default present in Windows. No need to add the explicitely.*/
#pragma comment(lib,"Msdmo.lib")
#pragma comment(lib,"dmoguids.lib")
#pragma comment(lib,"uuid.lib")
#pragma comment(lib,"amstrmid.lib")
#pragma comment(lib,"wmcodecdspuuid.lib")

#define SAFE_ARRAYDELETE(p) {if (p) delete[] (p); (p) = NULL;}
#define SAFE_RELEASE(p) {if (NULL != p) {(p)->Release(); (p) = NULL;}}

#define VBFALSE (VARIANT_BOOL)0
#define VBTRUE  (VARIANT_BOOL)-1
#define SUCCESS 1

extern std::ofstream g_log_file; //For logging perpose
void close_audio_file(FILE* fp)
{
	if (fp)
		fclose(fp);
}
#define CHECK_RET(hr, message, fileFp) if (FAILED(hr)) { puts(message); uninitialize_aec_rec();  close_audio_file(fileFp);  return DMO_REC_ERROR;}
#define CHECKHR(x, fileFp) hr = x; if (FAILED(hr)) {printf("%d: %08X\n", __LINE__, hr); uninitialize_aec_rec();  close_audio_file(fileFp); return DMO_REC_ERROR;}
#define CHECK_ALLOC(pb, message, fileFp) if (NULL == pb) { puts(message); uninitialize_aec_rec();  close_audio_file(fileFp); return DMO_REC_ERROR;}

IMediaObject* pDMO = NULL;
IPropertyStore* pPS = NULL;
AUDIO_DEVICE_INFO *pCaptureDeviceInfo = NULL, *pRenderDeviceInfo = NULL;
//Define audio file our format
WAVEFORMATEX wfxOut = { WAVE_FORMAT_PCM, 1, 8000, 16000, 2, 16, 0 };
extern std::ofstream g_log_file;
class CStaticMediaBuffer : public CBaseMediaBuffer {
public:
    STDMETHODIMP_(ULONG) AddRef() {return 2;}
    STDMETHODIMP_(ULONG) Release() {return 1;}
    void Init(BYTE *pData, ULONG ulSize, ULONG ulData) {
        m_pData = pData;
        m_ulSize = ulSize;
        m_ulData = ulData;
    }
};
CStaticMediaBuffer outputBuffer;
DMO_OUTPUT_DATA_BUFFER OutputBufferStruct = { 0 };

//Following two variables are added for notification
bool isRecordingInProgress = false;
std::mutex is_recording_in_progress;
std::mutex dmo_rec_ask_to_stop_rec_mtx;
bool isAskedToStopRecording = false;

//Thread
DWORD audioRecThreadId;
HANDLE audioRecThreadHndl;

HRESULT initialize_aec_rec()
{
    HRESULT hr = S_OK;
    CoInitialize(NULL);
	FILE *fp=NULL;
    
    
    OutputBufferStruct.pBuffer = &outputBuffer;
    DMO_MEDIA_TYPE mt = {0};

   

    // Parameters to config DMO
    int  iSystemMode = MODE_NOT_SET;    // AEC-MicArray DMO system mode
    int  iOutFileIdx = -1;              // argument index for otuput file name
    int  iMicDevIdx = -2;               // microphone device index
    int  iSpkDevIdx = -2;               // speaker device index
    BOOL bFeatrModeOn = 0;              // turn feature mode on/off
    BOOL bNoiseSup = 1;                 // turn noise suppression on/off
    BOOL bAGC = 0;                      // turn digital auto gain control on/off
    BOOL bCntrClip = 0;                 // turn center clippng on/off

     // microphone device index. The valid range is -1, 0~N-1
     // where N is the number of capture device. Use -1 if
     // you want to use the default device.
	 iMicDevIdx = -1;
     // speaker device index. The valid values are -1, 0~N-1
     // where N is the number of capture device. Use -1 if
     // you want to use the default device.
	 iSpkDevIdx = -1;
            
     // output file name
	 iOutFileIdx = 1;
	 // AEC-MicArray system mode. The valid modes are
	 //   SINGLE_CHANNEL_AEC = 0
	 //   OPTIBEAM_ARRAY_ONLY = 2
	 //   OPTIBEAM_ARRAY_AND_AEC = 4
	 //   SINGLE_CHANNEL_NSAGC = 5
	 //
	 // Mode 1 and 3 are reserved for future features.
	 iSystemMode = 0;


    HANDLE currThread;
    HANDLE currProcess;
    BOOL iRet;
    currProcess = GetCurrentProcess ();
    currThread = GetCurrentThread ();

    iRet = SetPriorityClass (currProcess, HIGH_PRIORITY_CLASS);
    if ( 0 == iRet )
    {
        // call getLastError.
		std::cout << "failed to set process priority\n";
		uninitialize_aec_rec();
		return DMO_REC_ERROR;
    }

    // DMO initialization
    CHECKHR(CoCreateInstance(CLSID_CWMAudioAEC, NULL, CLSCTX_INPROC_SERVER, IID_IMediaObject, (void**)&pDMO),fp);
    CHECKHR(pDMO->QueryInterface(IID_IPropertyStore, (void**)&pPS),fp);

    // Select capture device
    UINT uCapDevCount = 0;
    UINT uRenDevCount = 0;
    char  pcScanBuf[256]= {0};

    hr = GetCaptureDeviceNum(uCapDevCount);
    CHECK_RET(hr, "GetCaptureDeviceNum failed",fp);

    pCaptureDeviceInfo = new AUDIO_DEVICE_INFO[uCapDevCount];
    hr = EnumCaptureDevice(uCapDevCount, pCaptureDeviceInfo);
    CHECK_RET(hr, "EnumCaptureDevice failed",fp);

    std::cout<<"\n Default device will be used for capturing \n";
    
    // Set AEC mode and other parameters
    // Not all user changeable options are given in this sample code.
    // Please refer to readme.txt for more options.

    // Set AEC-MicArray DMO system mode.
    // This must be set for the DMO to work properly
    puts("\nAEC settings:");
    PROPVARIANT pvSysMode;
    PropVariantInit(&pvSysMode);
    pvSysMode.vt = VT_I4;
    pvSysMode.lVal = (LONG)(iSystemMode);
    CHECKHR(pPS->SetValue(MFPKEY_WMAAECMA_SYSTEM_MODE, pvSysMode),fp);
    CHECKHR(pPS->GetValue(MFPKEY_WMAAECMA_SYSTEM_MODE, &pvSysMode),fp);
    printf("%20s %5d \n", "System Mode is", pvSysMode.lVal);
    PropVariantClear(&pvSysMode);

    // Tell DMO which capture and render device to use 
    // This is optional. If not specified, default devices will be used
    if (iMicDevIdx >= 0 || iSpkDevIdx >= 0)
    {
        PROPVARIANT pvDeviceId;
        PropVariantInit(&pvDeviceId);
        pvDeviceId.vt = VT_I4;
        pvDeviceId.lVal = (unsigned long)(iSpkDevIdx<<16) + (unsigned long)(0x0000ffff & iMicDevIdx);
        CHECKHR(pPS->SetValue(MFPKEY_WMAAECMA_DEVICE_INDEXES, pvDeviceId),fp);
        CHECKHR(pPS->GetValue(MFPKEY_WMAAECMA_DEVICE_INDEXES, &pvDeviceId),fp);
        PropVariantClear(&pvDeviceId);
    }

    if ( bFeatrModeOn )
    {
        // Turn on feature modes
        PROPVARIANT pvFeatrModeOn;
        PropVariantInit(&pvFeatrModeOn);
        pvFeatrModeOn.vt = VT_BOOL;
        pvFeatrModeOn.boolVal = bFeatrModeOn? VBTRUE : VBFALSE;
        CHECKHR(pPS->SetValue(MFPKEY_WMAAECMA_FEATURE_MODE, pvFeatrModeOn),fp);
        CHECKHR(pPS->GetValue(MFPKEY_WMAAECMA_FEATURE_MODE, &pvFeatrModeOn),fp);
        printf("%20s %5d \n", "Feature Mode is", pvFeatrModeOn.boolVal);
        PropVariantClear(&pvFeatrModeOn);

        // Turn on/off noise suppression
        PROPVARIANT pvNoiseSup;
        PropVariantInit(&pvNoiseSup);
        pvNoiseSup.vt = VT_I4;
        pvNoiseSup.lVal = (LONG)bNoiseSup;
        CHECKHR(pPS->SetValue(MFPKEY_WMAAECMA_FEATR_NS, pvNoiseSup),fp);
        CHECKHR(pPS->GetValue(MFPKEY_WMAAECMA_FEATR_NS, &pvNoiseSup),fp);
        printf("%20s %5d \n", "Noise suppresion is", pvNoiseSup.lVal,fp);
        PropVariantClear(&pvNoiseSup);

        // Turn on/off AGC
        PROPVARIANT pvAGC;
        PropVariantInit(&pvAGC);
        pvAGC.vt = VT_BOOL;
        pvAGC.boolVal = bAGC ? VBTRUE : VBFALSE;
        CHECKHR(pPS->SetValue(MFPKEY_WMAAECMA_FEATR_AGC, pvAGC),fp);
        CHECKHR(pPS->GetValue(MFPKEY_WMAAECMA_FEATR_AGC, &pvAGC),fp);
        printf("%20s %5d \n", "AGC is", pvAGC.boolVal);
        PropVariantClear(&pvAGC);

        // Turn on/off center clip
        PROPVARIANT pvCntrClip;
        PropVariantInit(&pvCntrClip);
        pvCntrClip.vt = VT_BOOL;
        pvCntrClip.boolVal = bCntrClip? VBTRUE : VBFALSE;
        CHECKHR(pPS->SetValue(MFPKEY_WMAAECMA_FEATR_CENTER_CLIP, pvCntrClip),fp);
        CHECKHR(pPS->GetValue(MFPKEY_WMAAECMA_FEATR_CENTER_CLIP, &pvCntrClip),fp);
        printf("%20s %5d \n", "Center clip is", (BOOL)pvCntrClip.boolVal);
        PropVariantClear(&pvCntrClip);
    }

    // Set DMO output format
    hr = MoInitMediaType(&mt, sizeof(WAVEFORMATEX));
    CHECK_RET(hr, "MoInitMediaType failed",fp);
    
    mt.majortype = MEDIATYPE_Audio;
    mt.subtype = MEDIASUBTYPE_PCM;
    mt.lSampleSize = 0;
    mt.bFixedSizeSamples = TRUE;
    mt.bTemporalCompression = TRUE;
    mt.formattype = FORMAT_WaveFormatEx;
    memcpy(mt.pbFormat, &wfxOut, sizeof(WAVEFORMATEX));
    
    hr = pDMO->SetOutputType(0, &mt, 0); 
    CHECK_RET(hr, "SetOutputType failed",fp);
    MoFreeMediaType(&mt);

	// Allocate streaming resources. This step is optional. If it is not called here, it
	// will be called when first time ProcessInput() is called. However, if you want to 
	// get the actual frame size being used, it should be called explicitly here.
	hr = pDMO->AllocateStreamingResources();
	CHECK_RET(hr, "AllocateStreamingResources failed",fp);

	// Get actually frame size being used in the DMO. (optional, do as you need)
	int iFrameSize;
	PROPVARIANT pvFrameSize;
	PropVariantInit(&pvFrameSize);
	CHECKHR(pPS->GetValue(MFPKEY_WMAAECMA_FEATR_FRAME_SIZE, &pvFrameSize),fp);
	iFrameSize = pvFrameSize.lVal;
	PropVariantClear(&pvFrameSize);

	//Now we're ready for recording
    
    return hr;
}

int createThreadForAudioiRecording()
{
	is_recording_in_progress.lock();
	if (isRecordingInProgress)
	{
		is_recording_in_progress.unlock();
		g_log_file << "Recording is already in progress" << std::endl;
		return DMO_REC_ALREADY_IN_PROGRESS;
	}
	
	isRecordingInProgress = true;
	is_recording_in_progress.unlock();

	audioRecThreadHndl = CreateThread(
		NULL,                   // default security attributes
		0,                      // use default stack size  
		(LPTHREAD_START_ROUTINE)record_aec_audio,       // thread function name
		NULL,          // argument to thread function 
		0,                      // use default creation flags 
		&audioRecThreadId);   // returns the thread identifier 

	if (audioRecThreadHndl != NULL) {
		std::cout << " Audio recording Thread started." << std::endl;
		return DMO_REC_SUCCESS;
	}
	else
		return DMO_REC_ERROR;

}

//Thread proc. Expected to run independently
DWORD record_aec_audio(LPVOID threadData)
{
	UNREFERENCED_PARAMETER(threadData); //Not unsed for this function.

		initialize_aec_rec();
		// control how long the Demo runs
		int  iDuration = 60;   // seconds
		int  cTtlToGo = 0;
		HRESULT hr;
		ULONG cbProduced = 0;
		DWORD dwStatus;

		FILE * pfMicOutPCM;  // dump output signal using PCM format

		DWORD cOutputBufLen = 0;
		BYTE *pbOutputBuffer = NULL;
		// allocate output buffer
		cOutputBufLen = wfxOut.nSamplesPerSec * wfxOut.nBlockAlign;
		pbOutputBuffer = new BYTE[cOutputBufLen];
		CHECK_ALLOC(pbOutputBuffer, "out of memory.\n", pfMicOutPCM);
		// --- PREPARE OUTPUT --- //
		if (NULL != _tfopen_s(&pfMicOutPCM, L"sample_out.pcm", _T("wb")))
		{
			puts("cannot open file for output.\n");
			return ERROR;
		}
		// number of frames to play
		cTtlToGo = iDuration * 100;

		// main loop to get mic output from the DMO
		puts("\nAEC-MicArray is running ... Press \"s\" to stop");
		

		//Check exclusive access isAskedToStopRecording
		dmo_rec_ask_to_stop_rec_mtx.lock();
		bool stopRecording = isAskedToStopRecording;
		dmo_rec_ask_to_stop_rec_mtx.unlock();
		while (stopRecording == false)
		{
			Sleep(5); //sleep 10ms

			do {
				outputBuffer.Init((byte*)pbOutputBuffer, cOutputBufLen, 0);
				OutputBufferStruct.dwStatus = 0;
				hr = pDMO->ProcessOutput(0, 1, &OutputBufferStruct, &dwStatus);
				CHECK_RET(hr, "ProcessOutput failed", pfMicOutPCM);

				if (hr == S_FALSE) {
					cbProduced = 0;
				}
				else {
					hr = outputBuffer.GetBufferAndLength(NULL, &cbProduced);
					CHECK_RET(hr, "GetBufferAndLength failed", pfMicOutPCM);
				}

				// dump output data into a file with PCM format.
				if (fwrite(pbOutputBuffer, 1, cbProduced, pfMicOutPCM) != cbProduced)
				{
					puts("write error");
					return ERROR;
				}
				//Check for updated values of isAskedToStopRecording
				dmo_rec_ask_to_stop_rec_mtx.lock();
				stopRecording = isAskedToStopRecording;
				dmo_rec_ask_to_stop_rec_mtx.unlock();
				if (stopRecording) //request to stop accepting more input from input device(in our case, its Mic)
					pDMO->Discontinuity(0);

			} while (OutputBufferStruct.dwStatus & DMO_OUTPUT_DATA_BUFFERF_INCOMPLETE);

			fflush(pfMicOutPCM);
			
			dmo_rec_ask_to_stop_rec_mtx.lock();
			stopRecording = isAskedToStopRecording;
			dmo_rec_ask_to_stop_rec_mtx.unlock();
		}
		if(pfMicOutPCM)
			fclose(pfMicOutPCM);
		SAFE_ARRAYDELETE(pbOutputBuffer);
		is_recording_in_progress.lock();
		isRecordingInProgress = false;
		is_recording_in_progress.unlock();
		uninitialize_aec_rec();
	
	return SUCCESS;
}
void uninitialize_aec_rec()
{
	
	SAFE_ARRAYDELETE(pCaptureDeviceInfo);
	SAFE_ARRAYDELETE(pRenderDeviceInfo);

	SAFE_RELEASE(pDMO);
	SAFE_RELEASE(pPS);

	CoUninitialize();
}


