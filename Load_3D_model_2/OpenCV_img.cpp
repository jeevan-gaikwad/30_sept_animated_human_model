#include <windows.h>
#include <stdio.h> 
#include "OpenCV_img.h"
#include "AwsRekognize.h"
VideoCapture cap(CAMERA_DEVICE_ID);
DWORD opencvImgCapThreadId;
HANDLE opencvImgCapThreadHndl;

extern std::ofstream g_log_file;
Mat frame;
Mat gray;
//Mat face;
Mat face_resized;
int xxxx, yyyy, x1, z1, x2, y2, x3, y3;
int yyy;
std::mutex opencv_img_capture_mtx;
bool gbKeepCapturingImg = false;

bool gbIsImageCaptureInProgress = false;
std::mutex is_image_capture_in_progress_mutex;
std::string g_temp_captured_image_name = "captured.jpg";
extern bool gbMimicFacialExpressions;
extern std::mutex mimic_facial_expressions_mutex;
extern std::string previouslyRekognizedFaceId;
//Thread function
typedef int(*fun_ptr)();
static void read_csv(const std::string& filename, std::vector<Mat>& images, std::vector<int>& labels, char separator)
{
	std::ifstream file(filename.c_str(), std::ifstream::in);
	if (!file)
	{
		std::string error_message = "No valid input file was given, please check the given filename.";
	}
	std::string line, path, classlabel;
	while (getline(file, line))
	{
		std::stringstream liness(line);
		getline(liness, path, separator);
		getline(liness, classlabel);
		if (!path.empty() && !classlabel.empty())
		{
			images.push_back(imread(path, 0));
			labels.push_back(atoi(classlabel.c_str()));
		}
	}
}
int initialize_opencv()
{
	if(!cap.isOpened())
		cap.open(CAMERA_DEVICE_ID);

	//check for successfull open
	if (!cap.isOpened()) {
		g_log_file << "Capture Device ID " << CAMERA_DEVICE_ID << "cannot be opened." << std::endl;
		return OCV_ERROR;
	}
	else
		g_log_file << "Camera opened successfully!" << std::endl;

	previouslyRekognizedFaceId = "EMPTY_FACE"; //detect face id in every new session
	return OCV_SUCCESS;
}

DWORD capture_image(LPVOID imgCaptureNotifyCallBack)
{
	//void (*fun_ptr)() = (fun_ptr)imgCaptureNotifyCallBack;
	Rect roi(340, 100, 270, 270);
	bool keepCapturing = false;
	//Check for exclusive access
	opencv_img_capture_mtx.lock();
	keepCapturing = gbKeepCapturingImg;
	opencv_img_capture_mtx.unlock();
	fun_ptr callback_ptr = (fun_ptr)imgCaptureNotifyCallBack;
	
	while (keepCapturing)
	{
		is_image_capture_in_progress_mutex.lock();
		gbIsImageCaptureInProgress = true;;
		is_image_capture_in_progress_mutex.unlock();
		if (cap.isOpened())
			bool b = cap.read(frame);
		else
			return OCV_ERROR;	
		//Prepare image name. This code is added for testing, to avoid overwritting same image. Remove this in case we want to have fix name

		imwrite(g_temp_captured_image_name, frame);
		g_log_file << "Image captured successfully." << std::endl;
		mimic_facial_expressions_mutex.lock();
		bool isMimicFacialExpressionsOn = gbMimicFacialExpressions;
		mimic_facial_expressions_mutex.unlock();
		int ret=callback_ptr();// Call user defined callback to notify. This callback will tell, when to stop this thread
		if (ret == AWS_REK_SUCCESS_FACE_DETECTED && !isMimicFacialExpressionsOn)
		{
			//Work is done. Safely return
			is_image_capture_in_progress_mutex.lock();
			gbIsImageCaptureInProgress = false;;
			is_image_capture_in_progress_mutex.unlock();
			opencv_img_capture_mtx.lock();
			gbKeepCapturingImg=false;;
			opencv_img_capture_mtx.unlock();
			uninitialize_opencv();
			return OCV_SUCCESS;
		}//else continue
		if (isMimicFacialExpressionsOn)
		{	//Capture images little faster
			Sleep(MIMIC_FACE_CAPTURE_IMAGE_INTERVAL_SECONDS * 1000); //expects value in mili seconds. 
		}else
			Sleep(CAPTURE_IMG_INTERVAL_SECONDS * 1000); //expects value in mili seconds. We dont want to capture images continuously
		
		is_image_capture_in_progress_mutex.lock();
		gbIsImageCaptureInProgress = false;;
		is_image_capture_in_progress_mutex.unlock();

		//Check for updated value
		opencv_img_capture_mtx.lock();
		keepCapturing = gbKeepCapturingImg;
		opencv_img_capture_mtx.unlock();
	}
	
	return 0;
}


int CreateThreadForOpenCVImgCapture(void * imgCaptureNotifyCallBack)
{
	
	opencvImgCapThreadHndl = CreateThread(
		NULL,                   // default security attributes
		0,                      // use default stack size  
		(LPTHREAD_START_ROUTINE)capture_image,       // thread function name
		imgCaptureNotifyCallBack,          // argument to thread function 
		0,                      // use default creation flags 
		&opencvImgCapThreadId);   // returns the thread identifier 

	if (opencvImgCapThreadHndl != NULL) {
		g_log_file << " OpenCV Image capture Thread started." << std::endl;
		return OCV_SUCCESS;
	}
	else
		return OCV_ERROR;

}

void uninitialize_opencv()
{
	if(cap.isOpened())
		cap.release();
}