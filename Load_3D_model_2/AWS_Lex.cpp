#include <iostream>
#include "AWS_Lex.h"
#include <aws/core/Aws.h>
#include <aws\core\utils\FileSystemUtils.h>
#include <aws\core\utils\Outcome.h>
#include <aws/lex/model/PostContentResult.h>
#include <aws/lex/LexRuntimeServiceClient.h>
#include <aws/lex/LexRuntimeServiceRequest.h>
#include <aws/lex/model/PostContentRequest.h>
#include <aws/lex/model/PostTextRequest.h>
#include <aws/lex/LexRuntimeServiceErrors.h>
#include <aws/core/auth/AWSCredentialsProvider.h>
#include <aws/core/utils/logging/DefaultLogSystem.h>
#include <aws/core/utils/logging/AWSLogging.h>
#include <aws\core\http\HttpRequest.h>
#include<stdlib.h>
#include <fstream>
/**
* Test AWS Lex audio and Text
*/

#pragma comment(lib,"aws-cpp-sdk-core.lib")
#pragma comment(lib,"aws-cpp-sdk-lex.lib")


static const char* ALLOCATION_TAG = "LexSample::Main";
const Aws::String bot_name = "Vertexia";
const Aws::String bot_alias = "Vertexa";
const Aws::String user_id = "zodgevaibhav";
const Aws::String region_name = "eu-west-1";
//const Aws::String bot_name = "Trinity";
//const Aws::String bot_alias = "Trinity";
//const Aws::String user_id = "jeevan_gaikwad";
//const char* Aws::Http::CONTENT_TYPE_HEADER = "content-type";

Aws::SDKOptions lex_init_options;
std::shared_ptr<Aws::LexRuntimeService::LexRuntimeServiceClient> lex_client;
extern std::ofstream g_log_file; //FOr logging purpose
void make_text_request(Aws::String &text_to_send, LexResponse &lexResponse)
{
	Aws::LexRuntimeService::Model::PostTextRequest lex_post_text_content_request;
	lex_post_text_content_request.WithBotName(bot_name).WithBotAlias(bot_alias).WithUserId(user_id);
	lex_post_text_content_request.SetInputText(text_to_send);
	

	Aws::Utils::Outcome<Aws::LexRuntimeService::Model::PostTextResult, Aws::Client::AWSError<Aws::LexRuntimeService::LexRuntimeServiceErrors>>
		&outcome = lex_client->PostText(lex_post_text_content_request);
	Aws::LexRuntimeService::Model::PostTextResult& result = outcome.GetResult();
	auto error = outcome.GetError();


	g_log_file << "PostContent done: " << outcome.IsSuccess() << std::endl;
	g_log_file << "Error: " << error.GetMessage() << std::endl;
	//std::cout << "Intent: " << result.GetIntentName() << std::endl;
	g_log_file << "Message from Lex: " << result.GetMessage() << std::endl;
	//Return response as our param
	lexResponse.isSuccess = outcome.IsSuccess();
	lexResponse.errorMessage = error.GetMessage();
	lexResponse.intent = result.GetIntentName();
	lexResponse.message = result.GetMessage();
}

int make_audio_request_from_file(const std::string &audio_file_name, LexResponse &lexResponse)
{
	Aws::LexRuntimeService::Model::PostContentRequest lex_post_audio_content_request;
	
	lex_post_audio_content_request.WithBotName(bot_name.c_str())
								.WithBotAlias(bot_alias.c_str()).WithUserId(user_id.c_str()).WithAccept("audio/pcm").SetContentType("audio/lpcm; sample-rate=8000; sample-size-bits=16;channel-count=1; is-big-endian=false");
	
	auto input_audio_stream = Aws::MakeShared<Aws::FStream>("LexInputAudioStream",
		audio_file_name.c_str(), std::ios_base::in | std::ios_base::binary);

	auto body = Aws::MakeShared<Aws::FStream>(audio_file_name.c_str(), audio_file_name.c_str(), std::ios::in | std::ios::binary);

	if (body == nullptr) {
		g_log_file << std::string("Cannot open file ");
		return LEX_ERROR;
	}
	else {
		g_log_file << "File opened successfully! ";
		body->seekg(0, std::ios::end);
		unsigned long size = body->tellg();
		body->seekg(0);
		g_log_file << "Body length: " << size << std::endl;
	}
	lex_post_audio_content_request.SetBody(body);
	
	//Send request to AWS Lex
	Aws::Utils::Outcome<Aws::LexRuntimeService::Model::PostContentResult, Aws::Client::AWSError<Aws::LexRuntimeService::LexRuntimeServiceErrors>>
		&outcome = lex_client->PostContent(lex_post_audio_content_request);

	
	Aws::LexRuntimeService::Model::PostContentResult& result = outcome.GetResult();
	result.GetAudioStream();
	Aws::Client::AWSError<Aws::LexRuntimeService::LexRuntimeServiceErrors> error = outcome.GetError();


	std::cout << "PostContent done: " << outcome.IsSuccess() << std::endl;
	
	std::cerr << "Error: " << error.GetMessage() << std::endl;
	std::cout << "Intent: " << result.GetIntentName() << std::endl;
	std::cout << "Slots: " << result.GetSlots() << std::endl;
	std::cout << "Message from Lex: " << result.GetMessage() << std::endl;
	//Return response as our param
	lexResponse.isSuccess = outcome.IsSuccess();
	lexResponse.errorMessage = error.GetMessage();
	lexResponse.intent = result.GetIntentName();
	lexResponse.message = result.GetMessage();
	input_audio_stream->close();
	body->close();
	//remove(audio_file_name.c_str());
	return LEX_SUCCESS;
}

int initialize_lex()
{
	lex_init_options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Debug;

	Aws::InitAPI(lex_init_options);
	{
		
		Aws::Client::ClientConfiguration clientConfiguration;
		clientConfiguration.region = region_name; //ireland ??
		//clientConfiguration.region = "us-east-1"; //ireland ??

		//Jeevan creds
		lex_client = Aws::MakeShared<Aws::LexRuntimeService::LexRuntimeServiceClient>
			(ALLOCATION_TAG, /*aws_creds, */clientConfiguration);
		if (lex_client != nullptr)
			return LEX_SUCCESS;
		else
			return LEX_ERROR;		

	}
}
void uninitialize_lex()
{
	Aws::ShutdownAPI(lex_init_options);

}
