#pragma once
#include "opencv2/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"
#include <thread>         // std::thread
#include <mutex>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace cv;
using namespace cv::face;

#define CAMERA_DEVICE_ID 0
#define CAPTURE_IMG_INTERVAL_SECONDS 3
#define MIMIC_FACE_CAPTURE_IMAGE_INTERVAL_SECONDS 2
#define OCV_SUCCESS 1
#define OCV_ERROR 0



int initialize_opencv();
static void read_csv(const std::string& filename, std::vector<Mat>& images, std::vector<int>& labels, char separator = ';');

//Thread function
DWORD capture_image(LPVOID threadData);
int CreateThreadForOpenCVImgCapture(void * imgCaptureNotifyCallBack);
void uninitialize_opencv();
