#pragma once
#include <aws/core/Aws.h>
#include <aws/text-to-speech/TextToSpeechManager.h>
#include <iostream>
#include<Windows.h>


static const char* ALLOCATION_TAG = "PollySample::Main";

int pollySpeakText(std::string textToPlay);
void SelectVoiceAndOutputSound(const std::shared_ptr<Aws::TextToSpeech::TextToSpeechManager>& manager, std::string textToPlay);
