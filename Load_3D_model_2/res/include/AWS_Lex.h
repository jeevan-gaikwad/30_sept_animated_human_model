#pragma once
#include<iostream>
#include<aws\core\utils\memory\stl\AWSString.h>
#define LEX_ERROR 0
#define LEX_SUCCESS 1
typedef struct LexResponse
{
	bool isSuccess;
	Aws::String errorMessage;
	Aws::String intent;
	Aws::String message;
}LexResponse;

int  initialize_lex();
int  make_audio_request_from_file(const std::string &audio_file_name, LexResponse &lexResponse);
void make_text_request(Aws::String &text_to_send, LexResponse &lexResponse);
void uninitialize_lex();