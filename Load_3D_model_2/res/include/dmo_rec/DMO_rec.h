#pragma once
#include<Windows.h>

#define DMO_REC_SUCCESS 1
#define DMO_REC_ERROR 0
#define DMO_REC_ALREADY_IN_PROGRESS 2

HRESULT initialize_aec_rec();
int     createThreadForAudioiRecording();
DWORD   record_aec_audio(LPVOID threadData);
void    uninitialize_aec_rec();