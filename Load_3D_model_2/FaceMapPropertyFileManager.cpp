#include "FaceMapPropertyFileManager.h"

std::ofstream myfile;

void writeFaceDataToPropertyFile(char* faceId, char *personName)
{
	myfile.open("faces_lookup.txt", std::ios::app);
	myfile << "\n" << faceId << "," << personName;
	myfile.close();
}

void updateFaceMapFromPropertyFile(std::map<std::string, std::string>* faceData)
{
	std::ifstream myfile("faces_lookup.txt");
	std::string line;
	if (myfile.is_open())
	{

		while (std::getline(myfile, line))
		{
			std::stringstream ss(line);

			std::string key, value;
			std::getline(ss, key, ',');
			std::getline(ss, value, ',');
			faceData->insert(std::pair<std::string, std::string>(key, value));
		}
		myfile.close();
	}
	else std::cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Unable to open file";
}
