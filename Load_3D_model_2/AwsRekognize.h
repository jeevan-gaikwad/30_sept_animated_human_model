#pragma once
#include <aws\core\Aws.h>
#include<aws\core\utils\FileSystemUtils.h>
#include<aws\core\utils\Outcome.h>
#include<aws/core/auth/AWSCredentialsProvider.h>
#include <aws/core/utils/logging/DefaultLogSystem.h>
#include <aws/core/utils/logging/AWSLogging.h>
#include<aws\core\http\HttpRequest.h>

#include<aws\rekognition\RekognitionClient.h>
#include<aws\rekognition\model\SearchFacesByImageRequest.h>
#include<aws\rekognition\model\DetectFacesRequest.h>
#include<aws\rekognition\model\Image.h>
#include<aws\rekognition\model\IndexFacesRequest.h>
#define AWS_REK_ERROR 0
#define AWS_REK_SUCCESS_FACE_DETECTED 1
#define AWS_REK_FACE_NOT_FOUND 2

void createCollection(void);
void initSdk(void);
void searchFaces(const char* imageName, std::vector<std::string>& faceIds, Aws::Vector<Aws::Rekognition::Model::FaceDetail> &faceDetailsOut);
void recognizeImage(const char* imageName, std::vector<std::string>& faceIds, Aws::Vector<Aws::Rekognition::Model::FaceDetail> &faceDetailsOut);
void addFacesToCollection(const char* personName, std::vector<Aws::String>& outFaceIds);
void setEmotions(Aws::Rekognition::Model::Emotion emotion);
int ImageCapturedNotifyCallback();

std::string getGender(Aws::Rekognition::Model::Gender gender);
