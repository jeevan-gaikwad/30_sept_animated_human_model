#include "AwsRekognize.h"
#include <fstream>
#include "Utilities.h"
#include "AWS_Lex.h"
#include "AwsPollyClient.h"
#include "Animation.h"
#include "FaceMapPropertyFileManager.h"
#pragma comment(lib,"aws-cpp-sdk-core.lib")
#pragma comment(lib,"aws-cpp-sdk-rekognition.lib")


static const char* ALLOCATION_TAG_REK = "RekognizeSample::Main";

Aws::SDKOptions options;
Aws::Client::ClientConfiguration clientConfiguration;
Aws::Rekognition::Model::S3Object s3Object;

Aws::Rekognition::Model::Image image;
extern std::ofstream g_log_file;;
extern std::string g_temp_captured_image_name;
extern Animation animation;
extern bool gbMimicFacialExpressions;
extern std::mutex mimic_facial_expressions_mutex;
//Map to stop faceId and name mapping. Will be filled from local faces_lookup.txt file.
map<std::string, std::string> faceMap;

std::string previouslyRekognizedFaceId = "EMPTY_FACE";
std::shared_ptr<Aws::Rekognition::RekognitionClient> rek_client;
std::shared_ptr<Aws::Rekognition::RekognitionClient> rek_client_add_collection;
const char* Aws::Http::CONTENT_TYPE_HEADER = "content-type";
//Forwad declarations
void initSdk(void);
void searchFaces(const char* imageName, std::vector<std::string>& faceIds, Aws::Vector<Aws::Rekognition::Model::FaceDetail> &faceDetailsOut);

Aws::Utils::Array<unsigned char> unIdentifiedPersonPhotoData;

bool usingRekCLient = false;

int ImageCapturedNotifyCallback()
{
	std::string faceID;

	while (usingRekCLient)
		Sleep(2000);
	usingRekCLient = true;

	std::vector<std::string> faceIds;
	Aws::Vector<Aws::Rekognition::Model::FaceDetail> faceDetails;
	LexResponse lexResponse;
	recognizeImage(g_temp_captured_image_name.c_str(), faceIds, faceDetails);

	mimic_facial_expressions_mutex.lock();
	bool isMimicFacialExpressionsOn = gbMimicFacialExpressions;
	mimic_facial_expressions_mutex.unlock();
	if (isMimicFacialExpressionsOn)
	{
		if (faceDetails.size() > 0)
		{
			setEmotions(faceDetails[0].GetEmotions()[0]);
		}
	}
	std::string helloString = "";
	Aws::String lexText;
	if (((faceIds.size() == 0) || (faceIds.size()>1 &&  faceIds[1] != previouslyRekognizedFaceId )) && (faceDetails.size()>0 && getGender(faceDetails[0].GetGender()) != "UNKNOWN"))
	{
		g_log_file << "Face Change detected....";
		if (faceIds.size() > 0)
		{

			for (int i = 0; i < faceIds.size(); i++)
			{
				if (faceMap.find(faceIds[i]) != faceMap.end())
				{
					std::string personName = faceMap.at(faceIds[i]);
					g_log_file << "Callback called: faceId - " << faceIds[i] << std::endl;
					previouslyRekognizedFaceId = faceIds[i];
					g_log_file << "Recognized person name is - " << personName << std::endl;
					helloString.append(personName.c_str());
					helloString.append(", ");
					lexText.append("Talk to ");
					lexText.append(helloString.c_str());
				}
			}
			make_text_request(lexText, lexResponse);
			pollySpeakText((std::string)lexResponse.message.c_str());
            usingRekCLient = false;
			return AWS_REK_SUCCESS_FACE_DETECTED; //Face detected. Time to shutdown the thread.
		}
		else
		{
			previouslyRekognizedFaceId = "EMPTY_FACE";
			lexText.append("Talk to Unknown Person");

			make_text_request(lexText, lexResponse);
			pollySpeakText((std::string)lexResponse.message.c_str());
            usingRekCLient = false;
			return AWS_REK_FACE_NOT_FOUND;
		}

	}
	else {
		if (faceDetails.size()>0)
		{
			g_log_file << "Same Face detected..... Previously detected face id is ";// << previouslyRekognizedFaceId;
		}
		else
		{
			previouslyRekognizedFaceId = "EMPTY_FACE";
			g_log_file << "No face detected.....";
		}
	}
	usingRekCLient = false;
}

void recognizeImage(const char* imageName, std::vector<std::string>& faceIds, Aws::Vector<Aws::Rekognition::Model::FaceDetail> &faceDetailsOut)
{
	initSdk();
	//void createCollection();
	std::string localImageId;
	searchFaces(imageName, faceIds, faceDetailsOut);
	//Aws::ShutdownAPI(options);
}


void searchFaces(const char* imageName, std::vector<std::string>& faceIds, Aws::Vector<Aws::Rekognition::Model::FaceDetail> &faceDetailsOut)
{

	auto body = Aws::MakeShared<Aws::FStream>(g_temp_captured_image_name.c_str(), g_temp_captured_image_name.c_str(),
		std::ios::in | std::ios::binary);

	unsigned long file_size;
	if (body == nullptr) {
		g_log_file << std::string("Cannot open file ") << g_temp_captured_image_name;
		return;
	}
	else {
		g_log_file << "File " << g_temp_captured_image_name<< " opened successfully! ";
		body->seekg(0, std::ios::end);
		file_size = body->tellg();
		body->seekg(0);
		std::cout << "Body length: " << file_size << std::endl;
	}
	std::vector<unsigned char> dataBuffer;
	dataBuffer.resize(file_size);

	body->read((char*)(&dataBuffer[0]), file_size);

	Aws::Utils::Array<unsigned char> byteBuffer(&dataBuffer[0], file_size);

	image.SetBytes(byteBuffer);

	Aws::Rekognition::Model::SearchFacesByImageRequest request;
	request.WithImage(image)
		.WithCollectionId("VertexaS3Collection") //collection needs to create using api's, which contain images with faces. Sample java code available https://github.com/zodgevaibhav/aws-lex-poly-rekgonition-demo
		.WithFaceMatchThreshold(70)
		.WithMaxFaces(2);

	Aws::Rekognition::Model::SearchFacesByImageResult result;
	//Before we send request to AWS, check if we're shutting down
	if (isProgramShuttingDown() == false)
		result = rek_client->SearchFacesByImage(request).GetResultWithOwnership();
	else
		return; //Stop the processing

	Aws::Vector<Aws::Rekognition::Model::FaceMatch> vecMatch = result.GetFaceMatches();
	faceIds.resize(vecMatch.size());
	for (int i = 0;i < vecMatch.size();i++) //if multiple faces rekognized 
	{
		Aws::Rekognition::Model::Face match = vecMatch[i].GetFace();
		g_log_file << std::endl << std::endl;
		g_log_file << "Image ID is : " << match.GetImageId() << std::endl;
		g_log_file << "Face id is : " << match.GetFaceId() << std::endl;
		g_log_file << "Confidence is : " << match.GetConfidence() << std::endl;
		g_log_file << "External Image ID is : " << match.GetExternalImageId() << std::endl;
		faceIds.push_back(match.GetFaceId().c_str());		


		//************************************ Detect Faces **************************************88
		Aws::Rekognition::Model::DetectFacesRequest detectFacesRequest;// = Aws::Rekognition::Model::DetectFacesRequest::;
		detectFacesRequest.WithImage(image).WithAttributes({ Aws::Rekognition::Model::Attribute::ALL });
		
		Aws::Rekognition::Model::DetectFacesResult detectFacesResult = rek_client->DetectFaces(detectFacesRequest).GetResultWithOwnership();
		Aws::Vector<Aws::Rekognition::Model::FaceDetail> faceDetails = detectFacesResult.GetFaceDetails();
		if(!faceDetails.empty())
			faceDetailsOut.push_back(faceDetails[0]);		
	}

	if (vecMatch.size() < 1) // if no face rekognized, we still want to analyze the face to check wether the person is Male or Female
	{
		unIdentifiedPersonPhotoData = byteBuffer;
		Aws::Rekognition::Model::DetectFacesRequest detectFacesRequest;// = Aws::Rekognition::Model::DetectFacesRequest::;
		detectFacesRequest.WithImage(image).WithAttributes({ Aws::Rekognition::Model::Attribute::ALL });

		Aws::Rekognition::Model::DetectFacesResult detectFacesResult = rek_client->DetectFaces(detectFacesRequest).GetResultWithOwnership();
		Aws::Vector<Aws::Rekognition::Model::FaceDetail> faceDetails = detectFacesResult.GetFaceDetails();
		if(faceDetails.size()>0)
		faceDetailsOut.push_back(faceDetails[0]);
	}


	
}

void initSdk()
{
	options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Debug;

	Aws::InitAPI(options);
	{

		Aws::String AK;
		Aws::String SK;

		char* buf = nullptr;
		size_t sz = 0;

		if (_dupenv_s(&buf, &sz, "AWS_ACCESS_KEY_ID") == 0 && buf != nullptr)
		{
			AK = buf;
		}
		if (_dupenv_s(&buf, &sz, "AWS_SECRET_ACCESS_KEY") == 0 && buf != nullptr)
		{
			SK = buf;
			free(buf);
		}
		clientConfiguration.region = "us-west-2";
		Aws::Auth::AWSCredentials aws_creds(AK, SK);
		rek_client = Aws::MakeShared<Aws::Rekognition::RekognitionClient>(ALLOCATION_TAG_REK, aws_creds, clientConfiguration);
		rek_client_add_collection = Aws::MakeShared<Aws::Rekognition::RekognitionClient>(ALLOCATION_TAG_REK, aws_creds, clientConfiguration);
	}
}

void addFacesToCollection(const char* personName, std::vector<Aws::String>& outFaceIds)
{
	while (usingRekCLient)
	{
		Sleep(3000);
	}
	usingRekCLient = true;

	initSdk();
	/*auto body = Aws::MakeShared<Aws::FStream>("captured.jpg", "captured.jpg", std::ios::in | std::ios::binary);

	unsigned long file_size;
	if (body == nullptr) {
	std::cerr << std::string("Cannot open file ");
	return;
	}
	else {
	std::cout << "File opened successfully! ";
	body->seekg(0, std::ios::end);
	file_size = body->tellg();
	body->seekg(0);
	std::cout << "Body length: " << file_size << std::endl;
	}
	std::vector<unsigned char> dataBuffer;
	dataBuffer.resize(file_size);

	body->read((char*)(&dataBuffer[0]), file_size);

	Aws::Utils::Array<unsigned char> byteBuffer(&dataBuffer[0], file_size);
	*/


	image.SetBytes(unIdentifiedPersonPhotoData);


	Aws::Rekognition::Model::IndexFacesRequest indexedFaceRequest;
	indexedFaceRequest.WithCollectionId("VertexaS3Collection")
		.WithImage(image)
		.WithExternalImageId(personName)
		.WithDetectionAttributes({ Aws::Rekognition::Model::Attribute::ALL });

	Aws::Rekognition::Model::IndexFacesResult indexedFaceResult = rek_client_add_collection->IndexFaces(indexedFaceRequest).GetResultWithOwnership();

	const Aws::Vector<Aws::Rekognition::Model::FaceRecord> faceRecords = indexedFaceResult.GetFaceRecords();
	for (Aws::Rekognition::Model::FaceRecord record : faceRecords)
	{
		if (faceMap.find(record.GetFace().GetFaceId().c_str()) == faceMap.end())
		{
			writeFaceDataToPropertyFile((char*)record.GetFace().GetFaceId().c_str(), (char*)personName);
			updateFaceMapFromPropertyFile(&faceMap);
		}
		outFaceIds.push_back(record.GetFace().GetFaceId());
	}
	if (faceRecords.size() == 0)
		g_log_file << "!!!!!!!!!!!!!!!!!! Unable to add face in collection for person name - " << personName;

	usingRekCLient = false;
}

std::string getGender(Aws::Rekognition::Model::Gender gender)
{
	std::string strGender = "UNKNOWN";

	switch (gender.GetValue())
	{
	case Aws::Rekognition::Model::GenderType::Male:
		strGender = "MALE";
		break;
	case Aws::Rekognition::Model::GenderType::Female:
		strGender = "FEMALE";
		break;
	default:
		break;
	}

	return strGender;
}

void setEmotions(Aws::Rekognition::Model::Emotion emotion)
{
	switch (emotion.GetType())
	{
	case Aws::Rekognition::Model::EmotionName::ANGRY:
		g_log_file << "Emotion detected - Angry";
		animation.animationSettings.expression.emotion = EXPRESSION_ANGREE;
		animation.animationSettings.expression.factor = 1;
		break;
	case Aws::Rekognition::Model::EmotionName::CALM:
		g_log_file << "Emotion detected - Calm";
		animation.animationSettings.expression.emotion = EXPRESSION_NEUTRAL;
		animation.animationSettings.expression.factor = 1;
		break;
	case Aws::Rekognition::Model::EmotionName::CONFUSED:
		g_log_file << "Emotion detected - Confused";
		animation.animationSettings.expression.emotion = EXPRESSION_DOUBT;
		animation.animationSettings.expression.factor = 1;
		break;
	case Aws::Rekognition::Model::EmotionName::HAPPY:
		g_log_file << "Emotion detected - Happy/Smile";
		animation.animationSettings.expression.emotion = EXPRESSION_SMILE;
		animation.animationSettings.expression.factor = 1;
		break;
	case Aws::Rekognition::Model::EmotionName::SAD:
		g_log_file << "Emotion detected - Sad";
		animation.animationSettings.expression.emotion = EXPRESSION_SAD;
		animation.animationSettings.expression.factor = 1;
		break;
	case Aws::Rekognition::Model::EmotionName::DISGUSTED:
		g_log_file << "Emotion detected - Disgusted";
		animation.animationSettings.expression.emotion = EXPRESSION_DISGUST;
		animation.animationSettings.expression.factor = 1;
		break;
	case Aws::Rekognition::Model::EmotionName::SURPRISED:
		g_log_file << "Emotion detected - Surprices";
		animation.animationSettings.expression.emotion = EXPRESSION_SURPRISE;
		animation.animationSettings.expression.factor = 1;
		break;
	case Aws::Rekognition::Model::EmotionName::UNKNOWN:
		g_log_file << "Emotion detected - Unknown/Neutral";
		animation.animationSettings.expression.emotion = EXPRESSION_NEUTRAL;
		animation.animationSettings.expression.factor = 1;
		break;
	default:
		break;
	}
}
