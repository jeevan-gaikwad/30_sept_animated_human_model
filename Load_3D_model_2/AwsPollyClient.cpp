#include "AwsPollyClient.h"
#include <aws/text-to-speech/TextToSpeechManager.h>
#include <aws/polly/model/SynthesizeSpeechRequest.h>
#include <aws/polly/model/DescribeVoicesRequest.h>
#include <aws/polly/model/SynthesizeSpeechResult.h>
#include <aws/core/utils/Outcome.h>
#include <fstream>
#include <mutex>
#include "Utilities.h"

#pragma comment(lib,"aws-cpp-sdk-polly.lib")
#pragma comment(lib,"aws-cpp-sdk-text-to-speech.lib")

using namespace Aws::Polly;
using namespace Aws::TextToSpeech;

//std::string gTextToPlay;
bool gbIsPlaybackInProgress = false;
std::mutex is_playback_in_progress_mutex;

extern std::ofstream g_log_file;;
extern Animation animation;
std::vector<LIP_SYNC_INPUT> speechMarksOut;
int pollySpeakText(std::string textToPlay)
{
	Aws::SDKOptions options;
	options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Debug;
	Aws::InitAPI(options);

	auto client = Aws::MakeShared<PollyClient>(ALLOCATION_TAG);
	auto manager = TextToSpeechManager::Create(client);

	SelectVoiceAndOutputSound(manager, textToPlay);
	//Aws::ShutdownAPI(options);
	return 0;
}

void onPollyAudioPlaybackCompletedCallback(const char* originalText, const Aws::Polly::Model::SynthesizeSpeechOutcome& outcome, bool playedSuccessfully)
{
	//Handle post processing here after we played our audio received from Polly
    //Set global playback progress flag to false
    is_playback_in_progress_mutex.lock();
    gbIsPlaybackInProgress = false;
    is_playback_in_progress_mutex.unlock();

}

void onPollySpeechMarksOperationCompletedCallback(const char* originalText, const Aws::Polly::Model::SynthesizeSpeechOutcome& outcome, bool playedSuccessfully)
{
	std::string speechMarks;
	std::string marks = "";
	if (outcome.IsSuccess())
	{
		g_log_file << "Polly speech marks Oper completed for our text:" << originalText << " SpeechMarks received: ";
		auto result = const_cast<Aws::Polly::Model::SynthesizeSpeechOutcome&>(outcome).GetResultWithOwnership();
		auto& stream = result.GetAudioStream();
		std::streamsize amountRead(0);
		int noOfChars = 0;
		unsigned char buffer[BUFF_SIZE];
		char destBuff[BUFF_SIZE];
		while (stream)
		{
			stream.read((char*)buffer, BUFF_SIZE);
			auto read = stream.gcount();			
			speechMarks.append((char*)buffer);
			amountRead += read;
			noOfChars += read;
		}
		speechMarks.copy(destBuff, noOfChars);
		destBuff[noOfChars] = '\0';
        g_log_file << "Raw string in the buffer:" << destBuff << std::endl;
		std::string vismeData(destBuff);
		//std::stringstre
		//strncpy_s(marks, speechMarks, noOfChars);
		g_log_file << vismeData << std::endl;
		speechMarksOut.clear();
		parse_speech_marks(vismeData, speechMarksOut);

		//Notify and set visemes for rendering
        animation.animationSettings.startLipSyncPlayback(speechMarksOut);//Set visme for facial expressions
	}
    else
    {
        g_log_file << "ERROR: Polly speech marks Oper completed for our text:" << originalText << " SpeechMarks received: ";
    }
}
void SelectVoiceAndOutputSound(const std::shared_ptr<Aws::TextToSpeech::TextToSpeechManager>& manager, std::string textToPlay)
{
	/*g_log_file << "***************** Available voices are: " << std::endl;
	for (auto& voice : manager->ListAvailableVoices())
	{
		g_log_file << voice.first << "    language: " << voice.second << std::endl;
	}*/

	manager->SetActiveVoice("Raveena");
	g_log_file << "Active voice: Raveena" << std::endl;
	SendTextCompletedHandler audioPlaybackCompletionHandler;
	audioPlaybackCompletionHandler = onPollyAudioPlaybackCompletedCallback;

	SendTextCompletedHandler sppechMarksCompletionHandler;
	sppechMarksCompletionHandler = onPollySpeechMarksOperationCompletedCallback;

	//Set audio playback is in progress to if not already
    //if (animation.animationSettings.lipSync.setVisemes(speechMarksOut, animation.current_time))//Set visme for facial expressions
                                                                                               //
	is_playback_in_progress_mutex.lock(); //Critical section starts
	if (gbIsPlaybackInProgress && !animation.animationSettings.lipSync.isCurrentPlaybackFinished()) {
		is_playback_in_progress_mutex.unlock(); //Critical section ends
		g_log_file << "Previous audio playback is in progress. Rejecting parallel playback request." << std::endl;
		return;
	}else		
		gbIsPlaybackInProgress = true;
	is_playback_in_progress_mutex.unlock(); //Critical section ends

	manager->SendTextToOutputDevice(textToPlay.data(), audioPlaybackCompletionHandler); //For Text to Audio outpur
	//Sleep(6000);
	manager->SendTextToOutputSpeechMarks(textToPlay.data(), sppechMarksCompletionHandler); //For Text to Speech marks. Check SpeechMark.txt file in output folder
	

																	   //	g_log_file << "Anything else?" << std::endl;
																	   //}
}
/*
//Following function can be extended to set default playback device
void speakText()
{
	auto client = Aws::MakeShared<PollyClient>(ALLOCATION_TAG);
	auto manager = TextToSpeechManager::Create(client);

	g_log_file << "*****************available devices are: " << std::endl;
	auto devices = manager->EnumerateDevices();

	for (auto& device : devices)
	{
		g_log_file << "[" << device.first.deviceId << "] " << device.first.deviceName << "   Driver: "
			<< device.second->GetName() << std::endl;
	}

	g_log_file << std::endl << std::endl;

	Aws::String deviceId = "0"; //first default device

	auto foundDevice = std::find_if(devices.begin(), devices.end(),
		[&deviceId](const OutputDevicePair& device)
	{ return device.first.deviceId == deviceId; });

	if (foundDevice == devices.end())
	{
		g_log_file << "Invalid device selection for polly." << std::endl;
		return;
	}

	SelectVoiceAndOutputSound(manager);
}
*/
