#pragma once
#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/PutObjectRequest.h>
#include <iostream>
#include <fstream>

int uploadImageToS3(Aws::String file_name);
void initializeS3Sdk(void);
void unInitializeS3Sdk(void);